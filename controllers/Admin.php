<?php
class Admin extends FrontController{
    public function __construct(){
        parent::__construct();
        if( $this->model->CheckUsers()!=2){header("Location: /Errors"); }
    }
    public function Index(){
           $this->Render("Admin/Admin");
    }
 
    public function BlackList()
    {
        $this->model->AddUserInBL();
        $this->Render("Admin/ListUserBL");
    }
    public function DellUserBL()
    {
        $this->model->DellUserInBL();
        $this->Render("Admin/DellUserBL");
    }
    public function AddType()
    {
        $this->model->AddType();
        $this->Render("Admin/AddType");
        
    }
    public function DellType()
    {
        $this->model->DellType();
        $this->Render("Admin/DellType");
    }
    public function AddArticle()
    {
        $this->model->AddArticle();
        $this->Render("Admin/AddArticle");
    }
    public function DellNotice()
    {
        $this->model->DellNotice();
        $this->Render("Admin/DellNotice");
    }
    public function DellArticle()
    {
        $this->model->DellArticle();
        $this->Render("Admin/DellArticle");
    }
}