<?php
class Notice extends FrontController{
    public function Index()
    {
        $this->Render("Notice/List");
    }    
    public function ItemId()
    {
        $this->Render("/Notice/ItemId");
    }
    public function CategoryId()
    {
        $this->Render("Notice/Category");
    }
    public function NewNotice()
    {
        $this->model->NewNotice();
        $this->Render("Notice/NewNotice");
    }
    
}